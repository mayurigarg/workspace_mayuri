var fs = require('fs');
var util = require('util');
var loginPage = require('../pages/LoginPage.js');
var homePage = require('../pages/HomePage.js');
var adminPage = require('../pages/AdminPage.js');
var data = require('../testData.json');
var lib = require('../CommonLib.js');
var originalTimeout;

//Test Suite
describe('ON196-To check export function',function()
{   
   beforeEach(function(){
    lib.rmDir(data['directory']);
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    loginPage.login(data['username'],data['password']);
    });

   //TC1
it('Export organisation and check for JSON files',function()
{
  browser.ignoreSynchronization = false;
  //To go on admin page
  homePage.onAdminPage(); 

  //to check if export option is present
  expect(adminPage.exportButton.isPresent()).toBeTruthy();
 
  adminPage.clickExport().then(function(){
    var filename = data['directory'];
    browser.driver.wait(function () {
      //read download directory
      var files = lib.readDirt(filename);
      if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
          var filePath = filename + '\\' + files[i];       
        }
      //get file extension
      if (typeof filePath !== 'undefined' && filePath.length > 0){
        var temp2 = lib.getFileExtension(filePath);
      }    
      //check file extension        
      if ( temp2 == 'json'){
         return filePath;
      }    
    }, 2000).then(function (files) {
        // to check if file has been downloaded
        expect(files).toBeDefined();
        console.log(files + " files dowloaded and verified ! Manual checks has to be done for file content ");    
    });        
  });
}); 

afterEach(function() {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  loginPage.logout();
});

});



