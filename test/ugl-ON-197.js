var util = require('util');
var loginPage = require('../pages/LoginPage.js');
var homePage = require('../pages/HomePage.js');
var adminPage= require('../pages/AdminPage');
var data = require('../testData.json');
var originalTimeout;

describe('ON197-Check Occupancy Toggle',function()
{ 
   beforeEach(function()
   {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    loginPage.login(data['username'],data['password']);
    adminPage.clickAddOrganization();  
   }
   );

//TC1
it("To check if occupancy toggle is disabled and not selected by default", function()
{
  browser.ignoreSynchronization = false;
  adminPage.switchOffRealtimeToggleIfNotbyDefault();
  Promise.all([
    expect(adminPage.occupancyToggle.isPresent()).toBeTruthy(),
    expect(adminPage.occupancyToggle.getAttribute('class')).toBe('ui-switch disabled'),
    browser.driver.actions().mouseMove(adminPage.occupancyToolTip).perform().then(function()
    {
      expect(adminPage.occupancyToolTip.getAttribute('data-title')).toBe('To enable occupancy, you need to enable the Realtime Traffic button.');
    })
  ]).then (function(done) 
  {
    done;
}).catch( function(err)
{
  console.log('error in running promises', err);
}) 
}); 

//TC2
it("Behaviour of occupancy after selecting RealTime Traffic", function()
{
  browser.ignoreSynchronization = false;
  adminPage.switchOnRealtimeToggle();
  expect(adminPage.occupancyToggle.getAttribute('class')).toBe('ui-switch');
  adminPage.clickOccupancyToggle().then(function()
  {
    expect(adminPage.occupancyToggle.getAttribute('class')).toBe('ui-switch chosen');
  })
});

afterEach(function() {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  loginPage.logout();
});

});