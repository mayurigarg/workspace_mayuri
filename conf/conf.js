var Jasmine2HtmlReporter = require('C:/Users/jgargma/AppData/Roaming/npm/node_modules/protractor-jasmine2-html-reporter');

exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['../test/*.js'],
    //to do global preparation for your tests
    onPrepare: function() {
     
      browser.ignoreSynchronization = true;
      browser.driver.manage().window().maximize();
        jasmine.getEnv().addReporter(
          new Jasmine2HtmlReporter({
            savePath: 'target/screenshots',
            fileName: 'report',
            fileNameDateSuffix: true
          })
        );
     },
     capabilities: {
      'browserName': 'chrome',
      'chromeOptions': {

          prefs: {
              download: {
                  'prompt_for_download': false,
                  'directory_upgrade': true,
                  'default_directory':  'C:\\Downloads'
              }
          }
      }
  },
  jasmineNodeOpts: {
    defaultTimeoutInterval: 2500000
    },
    /*multiCapabilities:[
     {
        browserName: 'firefox'
     },
     {
       browserName: 'chrome'
     }
    ]*/
     
};