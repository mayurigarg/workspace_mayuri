describe('Calculator Application',function()
{
    var firstnumber = element(by.model('first'));
    var secondnumber = element(by.model('second'));
    var gobutton = element(by.id('gobutton'));
    var result = element(by.className('ng-binding'));
    var history = element.all(by.repeater('result in memory'));

    beforeEach(function()
   {
        browser.ignoreSynchronization = false;
        browser.get('http://juliemr.github.io/protractor-demo/');
    });
    //tc1
    it('Actual Title should match expected Title and print title',function()
    {
       browser.getTitle().then(function(title)
        {
            console.log(title);
            expect(title).toEqual('Super Calculator');
           // return;
        }
    ).then(function()
      {
        firstnumber.sendKeys('1');
        secondnumber.sendKeys('5');
        gobutton.click();
        expect(result.getText()).toEqual('6');
        console.log("Numbers added");
     }
       ).then(function()
    {
        expect(history.count()).toEqual(1);
        console.log("History Compared");
    })
});

    //tc2
   /* it('Should add two numbers',function(){
        firstnumber.sendKeys('1');
        secondnumber.sendKeys('5');
        gobutton.click();
        expect(result.getText()).toEqual('6');
    });*/
}); 