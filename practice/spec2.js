describe('To count history',function()
{
    var firstnumber = element(by.model('first'));
    var secondnumber = element(by.model('second'));
    var gobutton = element(by.id('gobutton'));
    var result = element(by.className('ng-binding'));
    var history = element.all(by.repeater('result in memory'));

    beforeEach(function()
    {
        browser.get('http://juliemr.github.io/protractor-demo/');
    }
    );

    //function to add to numbers
    function add(a,b){
        firstnumber.sendKeys(a);
        secondnumber.sendKeys(b);
        gobutton.click();
    }

    //tc1
    it('check history', function()
    {
        add(1,2);
        browser.driver.sleep(4000);
        add(4,5);
        browser.driver.sleep(4000);
        expect(history.count()).toEqual(2);
    }
    );
});