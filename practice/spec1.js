describe('Enter Name', function()
{

    it('Enter name as Jerry', function()
    {
       browser.ignoreSynchronization = true;
       browser.get('https://www.angularjs.org');
       element(by.model('yourName')).sendKeys('Jerry');
    });
});
describe('Compare Title',function(){
it('Compare Expected & Actual Title',function()
{ browser.get('https://angularjs.org');
  expect(browser.getTitle()).toEqual('AngularJS — Superheroic JavaScript MVW Framework');
});
});