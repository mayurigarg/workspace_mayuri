var util = require('util');

var HomePage = function() {
    this.drpdown = element(by.css('[ng-click="appDropdownIsOpen = !appDropdownIsOpen"]'));
    this.selectAdmin = element(by.css('[ng-click="navigateToAdmin()"]'));
    var EC = protractor.ExpectedConditions;
    this.isClickable = EC.elementToBeClickable(this.drpdown);

    this.onAdminPage = function()
    {
        browser.wait(this.isClickable, 5000);
        this.drpdown.click();
        this.selectAdmin.click();
    }

};

module.exports = new HomePage();