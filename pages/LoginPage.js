var data = require('../testData.json');

var LoginPage = function() {
    this.username = element(by.model('vm.username'));
    this.password = element(by.model('vm.password'));
    this.signin = element(by.name('submitButton'));
    this.logindropdwn = element(by.css('[ng-class="{ \'open\' \: userProfileDropdownIsOpen }"]'));
    this.logoutOption = element(by.css('[ng-click="logout()"]'));

    this.login = function(username, password){
        browser.get(data['url']);  
        browser.waitForAngular();
        this.username.sendKeys(username);
        this.password.sendKeys(password); 
        this.signin.click();.1
        browser.waitForAngular();
    }

    this.logout = function(){
        this.logindropdwn.click();
        this.logoutOption.click();

    }
};

module.exports = new LoginPage();

