var util = require('util');
var homePage = require('../pages/HomePage');


var AdminPage = function() {
    this.exportButton = element(by.css('[ng-click="vm.exportOrganization(organization.id)"]'));
    this.addOrganization = element(by.css('[ng-click="vm.addOrganization()"]'));
    var realtimeTrafficToggle = element(by.css('[chosen="vm.org.subscriptions.realtime_traffic"]')).element(by.css('[ng-click="vm.toggle()"]'));
    this.occupancyToggle =  element(by.css('[chosen="vm.org.subscriptions.occupancy"]')).element(by.css('[ng-click="vm.toggle()"]'));
    this.occupancyToolTip = element(by.className('sticon sticon-info exclamation-mark occupancy-info'));

    this.clickExport = function()
    {
       return this.exportButton.click();
    }

    this.clickAddOrganization = function()
    {
        homePage.onAdminPage();
        return this.addOrganization.click();
    }

    this.clickOccupancyToggle = function()
    {
        return this.occupancyToggle.click();
    }

    /*this.clickRealtimeToggle = function()
    {
        return this.realtimeTrafficToggle.click();
    }*/

    this.switchOffRealtimeToggleIfNotbyDefault = function()
    {
     
     realtimeTrafficToggle.getAttribute('class').then( function(value)
     {
      if(value == 'ui-switch chosen')
      {
        realtimeTrafficToggle.click();
      }
      });
    }

    this.switchOnRealtimeToggle = function()
    {
     
     realtimeTrafficToggle.getAttribute('class').then( function(value)
     {
      if(value != 'ui-switch chosen')
      {
        realtimeTrafficToggle.click();
      }
      });
    }

};

module.exports = new AdminPage();