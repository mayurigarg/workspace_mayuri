var util = require('util');
var fs = require('fs');
  
var CommonLib = function() {

  //To read directory
   this.readDirt = function(dirPath)  {
    try 
    { var files = fs.readdirSync(dirPath); }
    catch(e) 
    { 
      return; 
    }
    return files;
  };
  
  //To get downloaded file extension
   this.getFileExtension = function(filename) {
    return filename.split('.').pop();
  };
  
  //To remove all files and directories within dowload path
   this.rmDir = function(dirPath) {
     var files = this.readDirt(dirPath);
    if (files.length > 0)
      for (var i = 0; i < files.length; i++) 
      {
        var filePath = dirPath + '/' + files[i];
        if (fs.statSync(filePath).isFile())
          fs.unlinkSync(filePath);
        else
          rmDir(filePath);
      }
    }; 
  };

  module.exports = new CommonLib();